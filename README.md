# INSTRUCCIONES #

This README would normally document whatever steps are necessary to get your application up and running.

### Para trabajar con el proyecto ###

* Windows -> Instalar Git for Windows
* OSX -> Instalar Homebrew, despues desde consola instalar Git en caso de OSX


### Para empezar ###

## Clonar el proyecto 

* Abrir la consola
* Escribir "cd *ubicacion-donde-se-quiere-usar-como-workspace*" (opcional)
* Si no haces el paso anterior tu workspace va a ser el lugar donde estas parado en la consola
* Escribir "git clone url-del-repositorio"

