﻿using UnityEngine;
using System.Collections;
using System;

public class LivingEntity : MonoBehaviour, IDamageable {

    public float startingHealth;
    protected float health;
    protected bool dead;

    public event System.Action OnDeath;


    protected virtual void Start() {
        health = startingHealth;
    }

    public void TakeHit(float damage, RaycastHit hit)  {
        //do some stuff with hit :v
        TakeDamage(damage);
    }

    public void TakeDamage(float damage) {
        health -= damage;

        if (health <= 0 && !dead)  {
            Die();
        }
    }

    protected void Die() {
        dead = true;
        if (OnDeath != null) {
            OnDeath ();
        }
        GameObject.Destroy (gameObject);
    }

}
